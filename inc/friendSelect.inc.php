	<div id="content-wrapper">
		<p>S&eacute;lectionner vos amis :</p>
		<form id="e_friendSelect" action="" method="post">
		<?php foreach($_SESSION['e_profile']['friends'] as $friend){ ?>
			<div class="e_friendButton">
				<img alt="<?php echo $friend['name']; ?>" src="<?php echo $friend['picture']; ?>" />
				<input type="checkbox" id="<?php echo $friend['id']; ?>" name="friend" value="<?php echo $friend['id']; ?>" />
				<p>
					<label for="<?php echo $friend['id']; ?>">
					<?php 
					if($module->length($friend['name']) > 20){echo substr($friend['name'],0,18).'...';}
					else{echo $friend['name'];}
					?>
					</label>
					<span>
					<?php 

					if($module->length($friend['location']['name']) > 20){echo substr($friend['location']['name'],0,18).'...';}
					else{echo $friend['location']['name'];}
					?>
					</span>
				</p>
				<div style="clear:both;"></div>
			</div>
		<?php } ?>
		</form>
		
		<div class="e_buttonContainer">	
			<a href="index.php?mdl=ellipse&action=e_eventType" class="nextStep form">What are you in the mood for ?!</a>
			<a href="" class="nextStep form">Back !</a>
		</div>

	</div>
	