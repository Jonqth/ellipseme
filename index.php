<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Document sans titre</title>
<link href="css/main.css" type="text/css" rel="stylesheet" />
<script src="js/mobile.js" type="application/javascript"></script>
</head>

<body>
	<div id="e_wrapper">
		<?php include('inc/header.inc.php'); ?>
		<?php include('inc/friendSelect.inc.php'); ?>
        <?php include('inc/footer.inc.php'); ?>
    </div>
</body>

</html>